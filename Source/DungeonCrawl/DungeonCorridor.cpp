// Fill out your copyright notice in the Description page of Project Settings.

#include "DungeonCorridor.h"

void ADungeonCorridor::Init(const ADungeonGenerator* dGen, const ADungeonRoom* fromRoom, FVector inDirection, RandomStreamSeed* rStream)
{
	FVector size = fromRoom->RoomSize;
	FVector location = fromRoom->GetActorLocation();
	if (inDirection.X != 0)
	{
		location.X += (fromRoom->RoomSize.X / 2.0f) * inDirection.X;
		location.X += size.X/2.0f * inDirection.X;
		size.Y /= 2.0f;
	}
	else
	{
		location.Y += (fromRoom->RoomSize.Y / 2.0f) * inDirection.Y;
		location.Y += size.Y / 2.0f * inDirection.Y;
		size.X /= 2.0f;
	}
	
	Super::Init(dGen, location, size, rStream, ADungeonRoom::PillarLayout::NONE);
}


