// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "DungeonFloorCollision.generated.h"

UCLASS()
class DUNGEONCRAWL_API ADungeonFloorCollision : public AActor
{
	GENERATED_BODY()
	
public:	
	ADungeonFloorCollision(const FObjectInitializer& ObjectInitializer);

	void ModifySize(FVector size);

private:

	UPROPERTY(ReplicatedUsing = OnRep_SizeChange)
	FVector CollisionSize;

	UBoxComponent * boxCollision;
	 
    UFUNCTION()
    void OnRep_SizeChange();
};
