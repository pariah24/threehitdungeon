// Fill out your copyright notice in the Description page of Project Settings.

#include "DungeonGenerator.h"
#include "Engine/World.h"
#include "Engine/StaticMesh.h"
#include "AI/Navigation/NavMeshBoundsVolume.h"
#include "Kismet/GameplayStatics.h"
#include "DungeonCorridor.h"
#include "DungeonRoom.h"
#include "UnrealNetwork.h"

// Sets default values
ADungeonGenerator::ADungeonGenerator()
{
	PrimaryActorTick.bCanEverTick = true;
	worldGenerated = false;
}

void ADungeonGenerator::GenerateWithSeed(int32 seed, int32 fixedNumberOfRooms)
{
	//dungeon was already generated, do not duplicate
	if (worldGenerated)
		return;


	//create stream off given seed
	randStream.Init(seed);

	UWorld* world = GetWorld();
	int numberOfRooms = fixedNumberOfRooms > 0 ? fixedNumberOfRooms : randStream.RandRange(10, 15);

	
	//directions the dungeon can progress
	TArray<FVector> placementDirections = { FVector(1,0,0), FVector(0,-1,0), FVector(0,1,0) };

	//create positions for rooms
	TArray<FVector> roomPositions;
	FVector previousDirection = FVector::ZeroVector;
	roomPositions.Add(previousDirection); // first entry should be zero vector
	for (int i = 0; i < numberOfRooms - 1; i++)
	{
		//make sure we dont double back on ourselves. pick direction that isnt the opposite way we just came
		FVector nextDir;
		do
		{
			nextDir = placementDirections[randStream.RandRange(0, placementDirections.Num() - 1)];
		} while ((nextDir + previousDirection).IsZero());

		//add new room position
		previousDirection = nextDir;
		roomPositions.Add(nextDir);
	}

	//create rooms based off positions
	ADungeonRoom* previousRoom = nullptr;
	for (int i = 0; i < roomPositions.Num(); i++)
	{
		FVector direction = roomPositions[i];
		FVector roomSize = FVector(1000 + randStream.RandRange(0, 5) * 200, 1000 + randStream.RandRange(0, 5) * 200, 0);
		FVector roomPos = FVector::ZeroVector;

		//if we made a previous room, create a corridor from it to the one we are about to make
		if (previousRoom != nullptr)
		{
			ADungeonCorridor* createdCorridor = world->SpawnActor<ADungeonCorridor>();
			createdCorridor->Init(this, previousRoom, direction, &randStream);

			roomPos = createdCorridor->GetActorLocation();
			roomPos += (direction * createdCorridor->RoomSize * 0.5f);
			roomPos += (direction * roomSize * 0.5f);
		}

		//create room
		ADungeonRoom* createdRoom = world->SpawnActor<ADungeonRoom>();
		createdRoom->Init(this, roomPos, roomSize, &randStream, ADungeonRoom::PillarLayout::CORNERS);
		previousRoom = createdRoom;
		rooms.Add(createdRoom);

		//update dungeon bounds
		UpdateDungeonBounds(createdRoom);
	}

	//rebuild navmesh for generated level
	UNavigationSystem* NavSys = UNavigationSystem::GetCurrent(world);
	TArray<AActor*> NavVoumes;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ANavMeshBoundsVolume::StaticClass(), NavVoumes);
	
	if (Role >= ROLE_Authority && NavVoumes.Num() > 0)
	{
		ANavMeshBoundsVolume* navVolume = (ANavMeshBoundsVolume*)NavVoumes[0];
		navVolume->SetActorLocation((dungeonBoundMin + dungeonBoundMax) / 2.0f);

		//find dungeon size
		FVector dungeonSize = (dungeonBoundMax - dungeonBoundMin).GetAbs();
		dungeonSize.Z = 100;
		navVolume->SetActorScale3D(dungeonSize);

		//update navigation system
		NavSys->OnNavigationBoundsUpdated(navVolume);

		//spawn enemies
		for (int i = 1; i < rooms.Num(); i++)
		{
			rooms[i]->SpawnEnemies(2);
		}
	}


	//set generation flag
	worldGenerated = true;

	OnDungeonGenerated.Broadcast();
}

void ADungeonGenerator::UpdateDungeonBounds(const ADungeonRoom* newRoom)
{
	FVector min = newRoom->GetActorLocation() - (newRoom->RoomSize * 0.5f);
	FVector max = newRoom->GetActorLocation() + (newRoom->RoomSize * 0.5f);

	//set min
	if (min.X < dungeonBoundMin.X)
		dungeonBoundMin.X = min.X;
	if (min.Y < dungeonBoundMin.Y)
		dungeonBoundMin.Y = min.Y;
	
	//set max
	if (max.X > dungeonBoundMax.X)
		dungeonBoundMax.X = max.X;
	if (max.Y > dungeonBoundMax.Y)
		dungeonBoundMax.Y = max.Y;

}