// Fill out your copyright notice in the Description page of Project Settings.

#include "TPPlayerController.h"
#include "GameFramework/Character.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "PlayerCharacter.h"

ATPPlayerController::ATPPlayerController() : Super()
{

}

void ATPPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	//set input bindings
	InputComponent->BindAction("Teleport", IE_Pressed, this, &ATPPlayerController::TeleportButtonPressed);
}

void ATPPlayerController::TeleportButtonPressed()
{
	return;
	//perform raycast
	FHitResult res;
	GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, true, res);

	//get point to teleport to
	FVector positionToTeleport;
	if (res.bBlockingHit)
	{
		positionToTeleport = res.ImpactPoint + FVector(0, 0, GetCharacter()->GetCapsuleComponent()->GetScaledCapsuleHalfHeight());
	}

	APlayerCharacter* posessedPawn = (APlayerCharacter*)GetCharacter();
	FRotator rot = posessedPawn->GetActorRotation();

	//attempt the teleport
	posessedPawn->AttemptTeleport(positionToTeleport, rot);
}