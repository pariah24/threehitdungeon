// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DungeonRoom.h"
#include "DungeonCorridor.generated.h"


 //forward declarations
class ADungeonGenerator;
class ADungeonRoom;
class RandomStreamSeed;

UCLASS()
class DUNGEONCRAWL_API ADungeonCorridor : public ADungeonRoom
{
	GENERATED_BODY()
	
public:

	void Init(const ADungeonGenerator* dGen, const ADungeonRoom* fromRoom, FVector inDirection, RandomStreamSeed* rStream);
	
};
