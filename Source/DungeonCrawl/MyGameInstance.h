// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
//#include "OnlineSessionInterface.h"
#include "Sound/SoundCue.h"
#include "MyGameInstance.generated.h"

//Forward declarations
//class USoundCue;

UCLASS()
class DUNGEONCRAWL_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UMyGameInstance();

	//virtual void Init() override;

	UFUNCTION(BlueprintCallable, Category = "NetworkManagement")
	void StartServer();

	UFUNCTION(BlueprintCallable, Category = "NetworkManagement")
	void ConnectToServer(FString IpAddress);

	UFUNCTION(BlueprintCallable, Category = "NetworkManagement")
	void Disconnect();

	UFUNCTION(BlueprintCallable, Category = "NetworkManagement")
	void GoToGameMap(bool asSinglePlayer);

	UFUNCTION(BlueprintCallable, Category = "NetworkManagement")
	void GoToGameLobbyFromGame();

	UPROPERTY(BlueprintReadOnly, Category = "NetworkManagement")
	bool IsSinglePlayer;

	UFUNCTION(BlueprintCallable, Category = "Audio")
	void SetMusic(USoundCue* BackGroundMusicCue);

private:
	UAudioComponent* WorldMusicComponent;
};
