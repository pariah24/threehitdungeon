// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerMovementComponent.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.h"

/*
Definitions for UPlayerMovementComponent
*/

void UPlayerMovementComponent::ServerTeleportPawn_Implementation(FVector positionToTeleport, FRotator rot)
{
	bWantsToTeleport = true;
	TeleportPosition = positionToTeleport;
	rotAfterTeleport = rot;
}

bool UPlayerMovementComponent::ServerTeleportPawn_Validate(FVector positionToTeleport, FRotator rot)
{
	//atm we assume teleport is always valid.
	return true;
}


void UPlayerMovementComponent::AttemptTeleport(FVector from, FVector to)
{
	FSavedMovePtr d =  GetPredictionData_Client_Character()->AllocateNewMove();
	ServerTeleportPawn(to, FRotator());
}

void UPlayerMovementComponent::UpdateFromCompressedFlags(uint8 Flags)//Client only
{
	Super::UpdateFromCompressedFlags(Flags);
	return;
	bWantsToTeleport = (Flags&FSavedMove_Character::FLAG_Custom_0) != 0;
}

void UPlayerMovementComponent::OnMovementUpdated(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity)
{
	Super::OnMovementUpdated(DeltaSeconds, OldLocation, OldVelocity);
	return;
	if (!CharacterOwner)
	{
		return;
	}

	//send teleport action
	APlayerCharacter* character = (APlayerCharacter *)CharacterOwner;
	if (PawnOwner->IsLocallyControlled() && character->wantsToTeleport)
	{
		//consume teleport request
		character->wantsToTeleport = false;
		bWantsToTeleport = true;
		TeleportPosition = character->teleportPos;
		ServerTeleportPawn(TeleportPosition, character->GetActorRotation());
	}

	//perform teleport
	if (bWantsToTeleport)
	{
		if (PawnOwner->IsLocallyControlled())
		{
			UE_LOG(LogTemp, Warning, TEXT("play teleport"));
		}

		bWantsToTeleport = false;

		character->TeleportTo(TeleportPosition, rotAfterTeleport);
	}
}

class FNetworkPredictionData_Client* UPlayerMovementComponent::GetPredictionData_Client() const
{
	check(PawnOwner != NULL);
	//TODO
	//check(PawnOwner->Role < ROLE_Authority);

	return Super::GetPredictionData_Client();

	if (PawnOwner->Role >= ROLE_Authority)
	{
		//return Super::GetPredictionData_Client();
	}

	if (!ClientPredictionData)
	{
		UPlayerMovementComponent* MutableThis = const_cast<UPlayerMovementComponent*>(this);

		FNetworkPredictionData_Client_Character* baseSettings = (FNetworkPredictionData_Client_Character*)Super::GetPredictionData_Client();

		MutableThis->ClientPredictionData = (FNetworkPredictionData_Client_Character*)Super::GetPredictionData_Client();
		//MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_Player(this);
		/*MutableThis->ClientPredictionData->MaxSmoothNetUpdateDist = 92.f;
		MutableThis->ClientPredictionData->NoSmoothNetUpdateDist = 140.f;*/

		MutableThis->ClientPredictionData->MaxSmoothNetUpdateDist = baseSettings->MaxSmoothNetUpdateDist;
		MutableThis->ClientPredictionData->NoSmoothNetUpdateDist = baseSettings->NoSmoothNetUpdateDist;
		MutableThis->ClientPredictionData->MaxFreeMoveCount = baseSettings->MaxFreeMoveCount;
		MutableThis->ClientPredictionData->MaxMoveDeltaTime = baseSettings->MaxMoveDeltaTime;
		MutableThis->ClientPredictionData->MaxResponseTime = baseSettings->MaxResponseTime;
		MutableThis->ClientPredictionData->MaxSavedMoveCount = baseSettings->MaxSavedMoveCount;
	}
	//return Super::GetPredictionData_Client();
	return ClientPredictionData;
}

/*
Definitions for FSavedMove_Player
*/

void FSavedMove_Player::Clear()
{
	Super::Clear();
	return;
	bSavedWantsToTeleport = false;
	SavedTeleportPosition = FVector::ZeroVector;
}

uint8 FSavedMove_Player::GetCompressedFlags() const
{
	uint8 Result = Super::GetCompressedFlags();
	return Result;
	if (bSavedWantsToTeleport)
	{
		Result |= FLAG_Custom_0;
	}


	return Result;
}

bool FSavedMove_Player::CanCombineWith(const FSavedMovePtr & NewMove, ACharacter * Character, float MaxDelta) const
{

	//return Super::CanCombineWith(NewMove, Character, MaxDelta);
	return false;
}

void FSavedMove_Player::SetMoveFor(ACharacter * Character, float InDeltaTime, FVector const & NewAccel, FNetworkPredictionData_Client_Character & ClientData)
{
	Super::SetMoveFor(Character, InDeltaTime, NewAccel, ClientData);
	return;
	UPlayerMovementComponent* CharMov = Cast<UPlayerMovementComponent>(Character->GetCharacterMovement());
	if (CharMov)
	{
		bSavedWantsToTeleport = CharMov->bWantsToTeleport;
		SavedTeleportPosition = CharMov->TeleportPosition;
		SavedRotAfterTeleport = CharMov->rotAfterTeleport;
	}
}

void FSavedMove_Player::PrepMoveFor(ACharacter * Character)
{
	Super::PrepMoveFor(Character);
	return;
	UPlayerMovementComponent* CharMov = Cast<UPlayerMovementComponent>(Character->GetCharacterMovement());
	if (CharMov)// && bSavedWantsToTeleport)
	{
		CharMov->TeleportPosition = SavedTeleportPosition;		
		CharMov->rotAfterTeleport = SavedRotAfterTeleport;
	}
}



/*
Definitions for FNetworkPredictionData_Client_Player
*/

FNetworkPredictionData_Client_Player::FNetworkPredictionData_Client_Player(const UCharacterMovementComponent& ClientMovement)
	: Super(ClientMovement)
{

}

//FSavedMovePtr FNetworkPredictionData_Client_Player::AllocateNewMove()
//{
//	return FSavedMovePtr(new FSavedMove_Player());
//}
