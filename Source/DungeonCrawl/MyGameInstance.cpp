// Fill out your copyright notice in the Description page of Project Settings.

#include "MyGameInstance.h"
#include "Engine/Engine.h"
#include "Engine/World.h"
#include "AudioDevice.h"
#include "Kismet/GameplayStatics.h"

#define SETTINGS_MAPNAME TEXT("/Game/Levels/LobbyLevel")

UMyGameInstance::UMyGameInstance()
{
	IsSinglePlayer = true;
}
//
//void UMyGameInstance::Init()
//{
//
//}

void UMyGameInstance::StartServer()
{
	UE_LOG(LogTemp, Warning, TEXT("Starting server..."));

	UGameplayStatics::OpenLevel(GetWorld(), "LobbyLevel", true, "listen"); \
}

void UMyGameInstance::ConnectToServer(FString IpAddress)
{
	UE_LOG(LogTemp, Warning, TEXT("Connecting server %s ..."),* IpAddress);

	UWorld* world = GetWorld();
	APlayerController* pController = UGameplayStatics::GetPlayerController(world, 0);
	pController->ClientTravel(IpAddress, ETravelType::TRAVEL_Absolute);
}

void UMyGameInstance::Disconnect()
{
	UGameplayStatics::OpenLevel(GetWorld(), "MainMenu", true);
}

void UMyGameInstance::GoToGameMap(bool asSinglePlayer)
{
	IsSinglePlayer = asSinglePlayer;
	GetWorld()->ServerTravel("/Game/Levels/GameLevel");
}

void UMyGameInstance::GoToGameLobbyFromGame()
{
	GetWorld()->ServerTravel("/Game/Levels/LobbyLevel");
}

void UMyGameInstance::SetMusic(USoundCue * BackGroundMusicCue)
{
	//dont make a second time
	if (WorldMusicComponent)
		return;

	// Note this audio component is not attached to a world because it needs to persist between some level transitions
	UWorld* w = GetWorld();
	FAudioDevice* ad = w->GetAudioDevice();
	WorldMusicComponent = FAudioDevice::CreateComponent(BackGroundMusicCue, nullptr, nullptr, false, false);
	if (WorldMusicComponent)
	{
		WorldMusicComponent->bAllowSpatialization = false;
		WorldMusicComponent->bIsUISound = true;
		WorldMusicComponent->bIgnoreForFlushing = true;
		WorldMusicComponent->Play();
	}
}
