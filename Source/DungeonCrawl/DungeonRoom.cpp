// Fill out your copyright notice in the Description page of Project Settings.

#include "DungeonRoom.h"
#include "DungeonGenerator.h"
#include "DungeonFloorCollision.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Math/BoxSphereBounds.h"
#include "GenericUtilities.h"
#include "RandomStreamSeed.h"

// Sets default values
ADungeonRoom::ADungeonRoom()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
}

void ADungeonRoom::Init(const ADungeonGenerator* generator, FVector worldPos, FVector size, RandomStreamSeed* rStream, PillarLayout pLayout)
{
	dGen = generator;
	SetActorLocation(worldPos);
	ConstructForSize(size, rStream);
	SpawnPillars(pLayout);
}

void ADungeonRoom::ConstructForSize(FVector size, RandomStreamSeed* rStream)
{
	FVector finalTileScale = FVector::OneVector * (WorldTileSize / MeshTileSize);
	int gridSizeX = size.X / WorldTileSize;
	int gridSizeY = size.Y / WorldTileSize;
	RoomSize = FVector(gridSizeX * WorldTileSize, gridSizeY * WorldTileSize, 0);

	//create collision. we create a actor that is replicated to handle collison, otherwise networking prediction for character movement doesnt work properly
	UWorld * world = GetWorld();
	if (world->IsServer())
	{
		ADungeonFloorCollision* collision = world->SpawnActor<ADungeonFloorCollision>(GetActorLocation(), FRotator(0, 0, 0));
		collision->ModifySize(FVector(size.X, size.Y, 10) * 0.5f);
	}

	//offset so it's position is relative to parent and also so the origin of the room is at the center
	FVector worldOffset = GetActorLocation() - FVector((gridSizeX - 1) / 2.0f * WorldTileSize, (gridSizeY - 1) / 2.0f * WorldTileSize, 0);
	
	//creat tiles
	for (int x = 0; x < gridSizeX; x++)
	{
		for (int y = 0; y < gridSizeY; y++)
		{
			//create mesh component
			UStaticMeshComponent* meshC = NewObject<UStaticMeshComponent>(this);
			meshC->RegisterComponent();
			meshC->SetCollisionEnabled(ECollisionEnabled::NoCollision);

			//random mesh
			meshC->SetStaticMesh(dGen->floorTiles[rStream->RandRange(0, dGen->floorTiles.Num() - 1)]);
			
			//position with random offset
			FVector randomOffset = rStream->RandVector(-50, 50) * finalTileScale;
			randomOffset.Z = 0;
			meshC->SetWorldLocation(FVector(x * WorldTileSize, y * WorldTileSize, 0) + worldOffset + randomOffset);
			
			//random rotation
			FRotator rot = FRotator(0, rStream->RandRange(-15, 15), 0);
			meshC->SetRelativeRotation(rot);

			//mesh scale
			meshC->SetWorldScale3D(finalTileScale);
		}
	}
}

void ADungeonRoom::SpawnEnemies(int32 numberOfEnemies)
{
	UWorld* world = GetWorld();
	
	for (int i = 0; i < numberOfEnemies; i++)
	{
		FVector pos = GetActorLocation() + FVector::OneVector * i * 200 + FVector(0, 0, 100);
		AActor* enemy = world->SpawnActor(dGen->EnemyBP, &pos);
	}
}

void ADungeonRoom::SpawnPillars(PillarLayout pLayout)
{
	if (pLayout == PillarLayout::NONE)
		return;

	UWorld* world = GetWorld();
	TArray<FVector> pillarPositions;
	FVector meshBounds;
	TArray<UStaticMeshComponent*> meshComp;
	TArray<UClass*> tethting;
	dGen->PillarMesh.GetDefaultObject()->GetComponents<UStaticMeshComponent>(meshComp, true);
	GenericUtilities::FindDefaultComponentsByClass<UStaticMeshComponent>(dGen->PillarMesh.Get(), UStaticMeshComponent::StaticClass(), meshComp);

	if (meshComp.Num() > 0)
	{
		meshBounds = meshComp[0]->GetStaticMesh()->GetBoundingBox().GetSize() *  meshComp[0]->RelativeScale3D * 0.5f;
	}

	

	//get room positions
	pillarPositions.Add(FVector(RoomSize.X / 2, RoomSize.Y / 2, 0));
	pillarPositions.Add(FVector(-RoomSize.X / 2, -RoomSize.Y / 2, 0));
	pillarPositions.Add(FVector(-RoomSize.X / 2, RoomSize.Y / 2, 0));
	pillarPositions.Add(FVector(RoomSize.X / 2, -RoomSize.Y / 2, 0));

	//create pillars
	for (int i = 0; i < pillarPositions.Num(); i++)
	{
		FRotator rot = FRotator::ZeroRotator;
		FVector pos = GetActorLocation() + pillarPositions[i];

		//x offset to make sure its not partially out of the room
		if (pillarPositions[i].X < 0)
			pos.X += meshBounds.X;
		else if (pillarPositions[i].X > 0)
			pos.X -= meshBounds.X;

		//y offset to make sure its not partially out of the room
		if (pillarPositions[i].Y < 0)
			pos.Y += meshBounds.Y;
		else if (pillarPositions[i].Y > 0)
			pos.Y -= meshBounds.Y;

		//rotate
		if (pillarPositions[i].X > 0)
			rot = FRotator(0, 180, 0);


		world->SpawnActor(dGen->PillarMesh, &pos, &rot);
	}
}
