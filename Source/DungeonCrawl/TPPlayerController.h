// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TPPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class DUNGEONCRAWL_API ATPPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ATPPlayerController();


protected:
	void SetupInputComponent() override;
	
private:
	void TeleportButtonPressed();
};
