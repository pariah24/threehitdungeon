// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "PlayerMovementComponent.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class DUNGEONCRAWL_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter(const FObjectInitializer& PCIP);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void AttemptTeleport(FVector pos, FRotator rot);

	bool wantsToTeleport;
	FVector teleportPos;

private:

	//UFUNCTION(Server, Reliable, WithValidation)
	//void ServerTeleportPawn(FVector positionToTeleport, FRotator rot);
	void TeleportPawn(FVector positionToTeleport, FRotator rot);

private:
	UPlayerMovementComponent* moveComp;
};
