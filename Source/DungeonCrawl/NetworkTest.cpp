// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "NetworkTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DungeonCrawl, "DungeonCrawl" );

DEFINE_LOG_CATEGORY(LogNetworkTest)
 