// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomStreamSeed.h"

void RandomStreamSeed::Init(int32 seed)
{
	lastSeed = seed;
}

int32 RandomStreamSeed::RandRange(int32 from, int32 to)
{
	rStream.Initialize(lastSeed++);
	return rStream.RandRange(from, to);
}

FVector RandomStreamSeed::RandVector(float from, float to)
{
	rStream.Initialize(lastSeed++);
	return rStream.VRand() * rStream.FRandRange(from, to);
}