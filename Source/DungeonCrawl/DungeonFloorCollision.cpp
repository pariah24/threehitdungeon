// Fill out your copyright notice in the Description page of Project Settings.

#include "DungeonFloorCollision.h"
#include "Engine/World.h"
#include "UnrealNetwork.h"

ADungeonFloorCollision::ADungeonFloorCollision(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bReplicates = true;

	boxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	boxCollision->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	RootComponent = boxCollision;

}

void ADungeonFloorCollision::ModifySize(FVector size)
{
	//set size on server to be replicated to client, modify our collision box
	CollisionSize = size;
	boxCollision->SetBoxExtent(CollisionSize);
}

void ADungeonFloorCollision::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ADungeonFloorCollision, CollisionSize);
}

void ADungeonFloorCollision::OnRep_SizeChange()
{
	//modify our collision size when we get a new size
	boxCollision->SetBoxExtent(CollisionSize);
}

