// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DungeonRoom.generated.h"

//forward declarations
class ADungeonGenerator;
class RandomStreamSeed;

UCLASS()
class DUNGEONCRAWL_API ADungeonRoom : public AActor
{
	GENERATED_BODY()
	
public:	

	const float MeshTileSize = 250;

	enum PillarLayout { NONE, RANDOM, CORNERS };

	ADungeonRoom();

	void Init(const ADungeonGenerator* generator, FVector worldPos, FVector size, RandomStreamSeed* rStream, PillarLayout pLayout);
	void SpawnEnemies(int32 numberOfEnemies);

	UPROPERTY(EditAnywhere)
	float WorldTileSize = 100;

	UPROPERTY(BlueprintReadOnly)
	FVector RoomSize;

private:

	void ConstructForSize(FVector size, RandomStreamSeed* rStream);
	void SpawnPillars(PillarLayout pLayout);

	const ADungeonGenerator* dGen;
};
