// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Math/RandomStream.h"

/**
 * 
 */
class DUNGEONCRAWL_API RandomStreamSeed
{
public:

	void Init(int32 seed);
	int32 RandRange(int32 from, int32 to);
	FVector RandVector(float from, float to);

private:
	FRandomStream rStream;
	int32 lastSeed;
};
