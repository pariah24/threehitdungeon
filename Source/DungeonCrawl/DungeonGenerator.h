// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "RandomStreamSeed.h"
#include "DungeonGenerator.generated.h"

//forward declartions
class ADungeonRoom;
class UStaticMesh;

UCLASS()
class DUNGEONCRAWL_API ADungeonGenerator : public AActor
{
	GENERATED_BODY()

public:

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDungeonGenerated);

	ADungeonGenerator();

	UPROPERTY(EditAnywhere)
	TArray<UStaticMesh*> floorTiles;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> PillarMesh;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> EnemyBP;

	UFUNCTION(BlueprintCallable, Category = "WorldGeneration")
	void GenerateWithSeed(int32 seed, int32 fixedNumberOfRooms = -1);

	UPROPERTY(BlueprintAssignable, Category = "WorldGeneration")
	FDungeonGenerated OnDungeonGenerated;

private:
	
	void UpdateDungeonBounds(const ADungeonRoom* newRoom);

	TArray<ADungeonRoom*> rooms;

	bool worldGenerated;
	RandomStreamSeed randStream;
	FVector dungeonBoundMin;
	FVector dungeonBoundMax;
};
