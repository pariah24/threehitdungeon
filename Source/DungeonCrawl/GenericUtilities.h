// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"

/**
 * 
 */
class DUNGEONCRAWL_API GenericUtilities
{
public:

	template <class T>
	static void FindDefaultComponentsByClass(const UClass* InActorClass, UClass* InComponentClass, TArray<T*>& outArray);
};


template <class T>
void GenericUtilities::FindDefaultComponentsByClass(const UClass* InActorClass, UClass* InComponentClass, TArray<T*>& outArray)
{
	// Cast the actor class to a UBlueprintGeneratedClass
	const UBlueprintGeneratedClass* ActorBlueprintGeneratedClass = Cast<UBlueprintGeneratedClass>(InActorClass);

	// Use UBrintGeneratedClass->SimpleConstructionScript->GetAllNodes() to get an array of USCS_Nodes
	const TArray<USCS_Node*>& ActorBlueprintNodes = ActorBlueprintGeneratedClass->SimpleConstructionScript->GetAllNodes();

	// Iterate through the array looking for the USCS_Node whose ComponentClass matches the component you're looking for
	for (USCS_Node* Node : ActorBlueprintNodes)
	{
		if (UClass::FindCommonBase(Node->ComponentClass, InComponentClass) == InComponentClass)
		{
			// Return cast USCS node's Template into your component class and return it, data's all there
			outArray.Emplace(Cast<T>(Node->ComponentTemplate));
		}
	}
}