// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCharacter.h"

// Sets default values
APlayerCharacter::APlayerCharacter(const FObjectInitializer& PCIP)
	: Super(PCIP.SetDefaultSubobjectClass<UPlayerMovementComponent>(ACharacter::CharacterMovementComponentName))
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void APlayerCharacter::AttemptTeleport(FVector pos, FRotator rot)
{
	//moveComp->ResetPredictionData_Client();
	//moveComp->RecordTeleport(GetActorLocation(), pos);

	//perform local teleport, client side prediction
	//TeleportPawn(pos, rot);

	//inform server to perform teleport
	//ServerTeleportPawn(pos, rot);

	wantsToTeleport = true;
	teleportPos = pos;
}



/*
Teleport Pawn Networking code
*/

void APlayerCharacter::TeleportPawn(FVector positionToTeleport, FRotator rot)
{
	//TeleportTo(positionToTeleport, rot);
	//UCharacterMovementComponent* moveComp = (UCharacterMovementComponent*)GetCharacter()->GetMovementComponent();
}

//void APlayerCharacter::ServerTeleportPawn_Implementation(FVector positionToTeleport, FRotator rot)
//{
//	TeleportPawn(positionToTeleport, rot);
//}
//
//bool APlayerCharacter::ServerTeleportPawn_Validate(FVector positionToTeleport, FRotator rot)
//{
//	//atm we assume teleport is always valid.
//	return true;
//}

