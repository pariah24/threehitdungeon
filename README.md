# ThreeHitDungeon

Three hit dungeon is a dungeon crawler with the main purpose of experimenting with the Unreal engine and learning it's features. It is far from complete with many placeholder assets but primarily showcases random dungeon generation and server authorative multiplayer using client prediction. The idea is for you(and your friends) to make it to the end of the dungeon without dying. 3 hits is all you can take before you are eliminated from the round! NOTE: supports single player and multiplayer although multiplayer connects to host through direct ip. That means if machines are on different networks it might be hard establishing a connection without the use of a vpn.

Build:
Latest build is available under in the download tab in bitbucket

Controls:
WASD to move, mouse to change facing direction, left click to attack

Run in Editor:

- Unreal 1.8

- check out repo or download source manually

- Right click DungeonCrawl.uproject > Generate Visual Studio Project Files

- Open DungeonCrawl.sln

- Make sure build configuration is set to "Development Editor" and solution platform is set to "Win64" or "Win32"

- Sometimes the engine defaults to the startup project which causes build to fail. Right click on the DungeonCrawl project and chooise set as the startup project

- Run solution


